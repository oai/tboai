#
# Common OAI scripting library routines and other helpers.
#
package oailib;
use Exporter;
@ISA = "Exporter";
@EXPORT = qw (
    $OAI_DB_USER $OAI_DB_PASS $OAI_DB_NAME $MSISDN_PREFIX
    $PNET_KI $PNET_OPKEY $RAND_VAL $PNET_REALM $PNET_MCC $PNET_MNC $DEF_TAC
    $OAISIM_IMSI $OAISIM_MSISDN $OAISIM_IMEI $OAISIM_KEY $OAISIM_SQN
    $OAISIM_RAND $OAISIM_OPKEY

    loadManifest DBImport connectDB provisionRealUEs provisionSimUEs
    getMMEDBID getPGWDBID get_max_id
);

use strict;
use XML::LibXML;

# Exported Constants
our $OAI_DB_USER = "root";
our $OAI_DB_PASS = "linux";
our $OAI_DB_NAME  = "oai_db";

our $MSISDN_PREFIX = "3363806";
our $PNET_KI = "0x00112233445566778899AABBCCDDEEFF";
our $PNET_OPKEY = "01020304050607080910111213141516";
our $RAND_VAL = "0x00000000000000000000000000000000";
our $PNET_REALM = "phantomnet.org";
our $PNET_MCC = "998";
our $PNET_MNC = "98";
our $DEF_TAC = "1";

# Binh: OAISIM's eNodeB and UE subscriber information
our $OAISIM_IMSI = "000001234";
our $OAISIM_MSISDN = "33611111111";
our $OAISIM_IMEI = "35611302209414";
our $OAISIM_KEY = "0x8BAF473F2F8FD09487CCCBD7097C6862";
our $OAISIM_SQN = "00000281454575617153";
our $OAISIM_RAND = "0x0902263F8411A90F160A540F8950173A";
our $OAISIM_OPKEY = "0x8E27B6AF0E692E750F32667A3B14605D";

# Internal Constants
my $MYSQL_CLI = "/usr/bin/mysql";
my $GENIGET = "/usr/bin/geni-get";

# Global vars
my $dbh;

#
# Load the GENI manifest
#
sub loadManifest() {
    my $dom;

    my $manistr = `$GENIGET manifest`;
    chomp $manistr;
    die "Error fetching experiment manifest, or no manifest available!\n"
	if ($? || !$manistr);

    eval { $dom = XML::LibXML->load_xml(string => $manistr,
					load_ext_dtd => 0, 
					no_network => 1,
					line_numbers => 1) };

    die "Invalid XML in manifest: $@\n"
	if ($@);

    return $dom;
}

#
# RUN SQL statements from a file.
#
sub DBImport($$$$) {
    my ($user, $pass, $dbname, $sqlpath) = @_;
    require DBI;

    die "SQL file does not exist: $sqlpath"
	unless (-f $sqlpath);

    my $res = system("$MYSQL_CLI --user=$user --password=$pass $dbname < $sqlpath");
    if ($res) {
	$res = $res >> 8;
	die "Error running mysql client: $res";
    }

    return;
}

#
# Connect to a MySQL DB.
#
sub connectDB($$$) {
    my ($user, $pass, $dbname) = @_;
    require DBI;

    return DBI->connect("dbi:mysql:dbname=$dbname", $user, $pass,
			{ RaiseError => 1 });
}

#
# Get the maximum ID from a table (with an ID column).  Return '0'
# if no rows were found.
#
sub get_max_id($;$) {
    my ($tname, $idfield) = @_;    
    $idfield ||= "id";

    # Connect to the DB if necessary.
    $dbh = connectDB($OAI_DB_USER, $OAI_DB_PASS, $OAI_DB_NAME)
	if !$dbh;
    
    # Grab largest ID value from requested table.
    my $sth = $dbh->prepare("SELECT MAX($idfield) FROM $tname");
    $sth->execute();
    my ($id,) = $sth->fetchrow();
    $id ||= 0;
    $sth->finish();

    return $id;
}

sub getMMEDBID($$) {
    my ($mme_fqdn, $realm) = @_;

    # Connect to the DB if necessary.
    $dbh = connectDB($OAI_DB_USER, $OAI_DB_PASS, $OAI_DB_NAME)
	if !$dbh;

    my $sth = $dbh->prepare("SELECT idmmeidentity FROM mmeidentity WHERE".
			 "       mmehost='$mme_fqdn' and mmerealm='$realm'");
    $sth->execute();
    my ($mme_id,) = $sth->fetchrow();
    $sth->finish();
    
    return $mme_id;
}

sub getPGWDBID($) {
    my ($pgw_ipaddr,) = @_;

    # Connect to the DB if necessary.
    $dbh = connectDB($OAI_DB_USER, $OAI_DB_PASS, $OAI_DB_NAME)
	if !$dbh;
    
    my $sth = $dbh->prepare("SELECT id FROM pgw WHERE ipv4='$pgw_ipaddr'");
    $sth->execute();
    my ($pgw_id,) = $sth->fetchrow();
    $sth->finish();

    return $pgw_id;
}

#
# Add any real UEs found in the manifest to the provisioning DB.
#
sub provisionRealUEs($$$) {
    my ($mme_fqdn, $pgw_ipaddr, $realm) = @_;

    # Connect to the DB if necessary.
    $dbh = connectDB($OAI_DB_USER, $OAI_DB_PASS, $OAI_DB_NAME)
	if !$dbh;

    # Get various DB entity IDs.
    my $mme_id = getMMEDBID($mme_fqdn, $realm) || die "Failed to lookup mme_id!";
    my $pgw_id = getPGWDBID($pgw_ipaddr) || die "Failed to lookup pgw_id!";
    my $pdn_id = get_max_id("pdn") + 1 || die "Failed to get initial PDN id!";
    
    # Extract GENI manifest from the testbed database.
    my $mani = loadManifest();

    # Loop through nodes in manifest, looking for those with UE attributes.
    my $root = $mani->documentElement();
    my @nodes = $root->getChildrenByLocalName("node");
    foreach my $node (@nodes) {
	my $vname = $node->getAttribute("client_id");
	my $imsi  = $node->getAttribute("sim_imsi");
	my $seqno = $node->getAttribute("sim_sequence_number");
	next unless ($vname && $imsi && $seqno);
	
	print "Provisioning UE '$vname': $imsi, $seqno\n";
 
	# Pad the sequence number out to 20 digits with leading zeros. Why?
	# Good question for the OAI folks...
	$seqno = sprintf("%020d", $seqno);

	# Create fake MSISDN number. Use last four digits of IMSI.
	my $msisdn = $imsi;
	$msisdn =~ s/^\d{11}(\d{4})$/${MSISDN_PREFIX}$1/;

	# Not clear what all of these params mean or how they are used in
	# practice; most were just copied from the OAI wiki.
	$dbh->do(
	    "INSERT INTO users (".
	    "  `imsi`, `msisdn`, `imei`, `imei_sv`,".
	    "  `ms_ps_status`, `rau_tau_timer`, `ue_ambr_ul`, `ue_ambr_dl`,".
	    "  `access_restriction`, `mme_cap`, `mmeidentity_idmmeidentity`,".
	    "  `key`, `RFSP-Index`, `urrp_mme`, `sqn`, `rand`, `OPc`) ".
	    "VALUES (".
	    "  '$imsi', '$msisdn', NULL, NULL,".
	    "  'PURGED', '120', '50000000', '100000000',".
	    "  '47', '0000000000', '$mme_id',".
	    "  $PNET_KI, '1', '0', '$seqno', $RAND_VAL, '')");

	# Ditto above.  My guess is that all zero IP addresses indicate
	# dynamic allocation.  The pool is handled via a row in the `pgw`
	# table.
	$dbh->do(
	    "INSERT INTO pdn (".
	    "  `id`, `apn`, `pdn_type`, `pdn_ipv4`, `pdn_ipv6`,".
	    "  `aggregate_ambr_ul`, `aggregate_ambr_dl`, `pgw_id`, ".
	    "  `users_imsi`, `qci`, `priority_level`,`pre_emp_cap`,".
	    "  `pre_emp_vul`, `LIPA-Permissions`) ".
	    "VALUES (".
	    "  '$pdn_id', 'internet','IPV4', '0.0.0.0', '0:0:0:0:0:0:0:0',".
	    "  '50000000', '100000000', '$pgw_id',".
	    "  '$imsi', '9', '15', 'DISABLED',".
	    "  'ENABLED', 'LIPA-ONLY')");
	$pdn_id++;
    }

    return;
}

#
# Provision simulated UEs.  For now this is run regardless of whether or
# not users have actually allocated them in the experiment.
#
sub provisionSimUEs($$$) {
    my ($mme_fqdn, $pgw_ipaddr, $realm) = @_;

    # Connect to the DB if necessary.
    $dbh = connectDB($OAI_DB_USER, $OAI_DB_PASS, $OAI_DB_NAME)
	if !$dbh;

    # Get various DB entity IDs.
    my $mme_id = getMMEDBID($mme_fqdn, $realm);
    my $pgw_id = getPGWDBID($pgw_ipaddr);
    my $pdn_id = get_max_id("pdn") + 1;
    
    #
    #Binh: provisioning OAISIM's UE.
    #Always remove and reinstall the information because currently the 
    #OAISIM UE won't authenticate if the database is old.
    #
    my $oai_imsi = "$PNET_MCC$PNET_MNC$OAISIM_IMSI";

    $dbh->do(
	    "INSERT INTO users (".
	    "  `imsi`, `msisdn`, `imei`, `imei_sv`,".
	    "  `ms_ps_status`, `rau_tau_timer`, `ue_ambr_ul`, `ue_ambr_dl`,".
	    "  `access_restriction`, `mme_cap`, `mmeidentity_idmmeidentity`,".
	    "  `key`, `RFSP-Index`, `urrp_mme`, `sqn`, `rand`, `OPc`) ".
	    "VALUES (".
	    "  '$oai_imsi', '$OAISIM_MSISDN', NULL, NULL,".
	    "  'PURGED', '120', '50000000', '100000000',".
	    "  '47', '0000000000', '$mme_id',".
	    "  $OAISIM_KEY, '1', '0', '$OAISIM_SQN', $OAISIM_RAND, $OAISIM_OPKEY)");
    $dbh->do(
	    "INSERT INTO pdn (".
	    "  `id`, `apn`, `pdn_type`, `pdn_ipv4`, `pdn_ipv6`,".
	    "  `aggregate_ambr_ul`, `aggregate_ambr_dl`, `pgw_id`, ".
	    "  `users_imsi`, `qci`, `priority_level`,`pre_emp_cap`,".
	    "  `pre_emp_vul`, `LIPA-Permissions`) ".
	    "VALUES (".
	    "  '$pdn_id', 'internet','IPV4', '0.0.0.0', '0:0:0:0:0:0:0:0',".
	    "  '50000000', '100000000', '$pgw_id',".
	    "  '$oai_imsi', '9', '15', 'DISABLED',".
	    "  'ENABLED', 'LIPA-ONLY')");

    print "Provisioned OAISIM UE : $oai_imsi\n";

    return;
}

# One is the loneliest number...
1;
