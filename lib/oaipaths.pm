package tbpaths;
use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw (
    $BINDIR $ETCDIR $VARDIR $BOOTDIR $DBDIR $LOGDIR $LOCKDIR $BLOBDIR
    $DYNRUNDIR $STATICRUNDIR

    $OAI_ETCDIR $OAI_LIBDIR $OAI_BINDIR $OAI_EPCDIR $OAI_RANDIR 
    $OAI_LOGDIR $OAI_LOCKDIR $OAI_SYSETC 
);

our $BINDIR = "/usr/local/etc/emulab";
unshift(@INC, "/usr/local/etc/emulab");
our $ETCDIR = "";
if (-d "/etc/emulab") {
    $ETCDIR = "/etc/emulab";
}
else {
    $ETCDIR = "/usr/local/etc/emulab";
}
our $STATICRUNDIR = "/usr/local/etc/emulab/run";
our $VARDIR  = "/var/emulab";
our $BOOTDIR = "/var/emulab/boot";
our $LOGDIR  = "/var/emulab/logs";
our $LOCKDIR = "/var/emulab/lock";
our $DBDIR   = "/var/emulab/db";
our $BLOBDIR = $BOOTDIR;
our $DYNRUNDIR = "/var/run/emulab";

our $OAI_BASEDIR  = "/opt/oai";
our $OAI_PNDIR    = "$OAI_BASEDIR/phantomnet";
our $OAI_ETCDIR   = "$OAI_PNDIR/etc";
our $OAI_LIBDIR   = "$OAI_PNDIR/lib";
our $OAI_BINDIR   = "$OAI_PNDIR/bin";
our $OAI_EPCDIR   = "$OAI_BASEDIR/openair-cn";
our $OAI_RANDIR   = "$OAI_BASEDIR/openairinterface5g";
our $OAI_LOGDIR   = "/var/log/oai";
our $OAI_LOCKDIR  = "/var/lock";
our $OAI_SYSETC   = "/usr/local/etc/oai";

unshift(@INC, $OAI_LIBDIR);
# Try to make the OAI log directory if it doesn't exist.
if (!-d $OAI_LOGDIR) {
    system("mkdir $OAI_LOGDIR > /dev/null 2>&1");
}

# Perl goo
1;
