# Library of useful functions for OpenEPC

package epclib;
use Exporter;
@ISA    = "Exporter";
@EXPORT = qw ( 
    MGMT_SUBNET NETA_SUBNET NETB_SUBNET NETC_SUBNET NETD_SUBNET
    OPENEPC_ROLES OPENEPC_CLIENTS PNET_PLMNID IMSI_SUFFIX_LEN
    SIM_IMSI_SUFFIX_BASE
    get_emulab_controlif get_ifmap update_openepc_vars 
    elabnet_to_epcnet epcnet_to_elabnet mk_screenrc
    get_geni_parameters load_manifest populate_hss_subscribers
);

use strict;
use XML::LibXML;

# Drag in Emulab and PhantomNet paths.
BEGIN {
    require "/etc/emulab/paths.pm";
    import emulabpaths;
    require "/etc/phantomnet/paths.pm";
    import phantomnetpaths;
}

use libtmcc;
use libsetup;

my %ELAB2OEPC_NETS = (
    "mgmt" => "mgmt",
    "net-a" => "net_a",
    "net-b" => "net_b",
    "net-c" => "net_c",
    "net-d" => "net_d",
    "an-gprs" => "an_gprs",
    "an-umts" => "an_umts",
    "an-lte" => "an_lte",
    "an-wifi" => "an_wifi",
    "an-wimax" => "an_wimax",
    "enodeb-an-lte" => "net_c",
    "nodeb-an-umts" => "net_c",
    "epdg-an-wifi" => "net_c",
    "angw-an-wimax" => "net_c",
);

my %OEPC2ELAB_NETS = (
    "mgmt" => "mgmt",
    "net_a" => "net-a",
    "net_b" => "net-b",
    "net_c" => "net-c",
    "net_d" => "net-d",
    "an_gprs" => "an-gprs",
    "an_umts" => "an-umts",
    "an_lte" => "an-lte",
    "an_wifi" => "an-wifi",
    "an_wimax" => "an-wimax",
    "enodeb-net_c" => "an-lte",
    "nodeb-net_c" => "an-umts",
    "epdg-net_c" => "an-wifi",
    "angw-net_c" => "an-wimax",
);

sub OPENEPC_ROLES {
    return (
	"epc-enablers" => 1,
	"pgw" => 1,
	"enodeb" => 1,
	"sgw-mme-sgsn" => 1,
	"nodeb" => 1,
	"epdg" => 1,
	"angw" => 1,
	"epc-client" => 1,
	"any" => 1
	);
}

sub OPENEPC_CLIENTS {
    return (
	"alice" => 1,
	"bob" => 1
	);
}

sub MGMT_SUBNET() { return "192.168.254"; }
sub NETA_SUBNET() { return "192.168.1"; }
sub NETB_SUBNET() { return "192.168.2"; }
sub NETC_SUBNET() { return "192.168.3"; }
sub NETD_SUBNET() { return "192.168.4"; }

my $TOUCH = "/usr/bin/touch";
my $FINDIF = "$BINDIR/findif";
my $GENIGET = "/usr/bin/geni-get";
my $OPENEPC_VARFILE = "$OEPC_ETCDIR/configure_system_vars.sh";

my $debug = 0;
my %ifmap = ();

my $PASSDB = "$VARDIR/db/passdb";
my $SCREENRC_FILE = "/root/.screenrc";

my $PNET_PLMNID = "99898";
my $IMSI_LENGTH = 15; # e.g., 001010123456789
my $SIM_SQN_LENGTH = 12;
my $MSISDN_PREFIX = "12345678";
my $ADD_SUBSCRIBER = "$OEPC_PNDIR/provision_client.pl";
my $POPULATE_HSS_FFILE = "$BOOTDIR/OEPC_SUBSCRIBERS_ADDED";
my $GENI_PARAM_NS = "http://www.protogeni.net/resources/rspec/ext/profile-parameters/1";
my $GENI_PARAM_PREFIX = "emulab.net.parameter";
my $IMSI_SUFFIX_LEN = 10;
my $SIM_IMSI_SUFFIX_BASE = 1000;
my $SIM_INIT_SQN = 21;

sub PNET_PLMNID() { return $PNET_PLMNID; }
sub SIM_IMSI_SUFFIX_BASE() { return $SIM_IMSI_SUFFIX_BASE; }
sub IMSI_SUFFIX_LEN() { return $IMSI_SUFFIX_LEN; }

#
# Utility func for the following two lib functions
#
sub prefix_search ($$) {
    my ($str, $href) = @_;
    foreach my $key (keys %$href) {
	return $href->{$key} if $str =~ /^$key/;
    }
    return undef;
}

#
# Return the OpenEPC network name given the Emulab network name.
# Match against the first part (prefix) of the network name.
#
sub elabnet_to_epcnet($$) {
    my ($role, $elnet) = @_;

    my $combined = "${role}-${elnet}";
    return
	prefix_search($combined, \%ELAB2OEPC_NETS) || 
	prefix_search($elnet, \%ELAB2OEPC_NETS);
}

#
# Return the Emulab network name given the OpenEPC network name.
# Match against the first part (prefix) of the network name.
#
sub epcnet_to_elabnet($$) {
    my ($role, $epcnet) = @_;

    my $combined = "${role}-${epcnet}";
    return
	prefix_search($combined, \%OEPC2ELAB_NETS) || 
	prefix_search($epcnet, \%OEPC2ELAB_NETS);
}

#
# Grab control net interface name.
#
sub get_emulab_controlif() {
    my $ctrlif = undef;

    if (-e "$BOOTDIR/controlif") {
	if (!open(CIF, "< $BOOTDIR/controlif")) {
	    warn "Can't open $BOOTDIR/controlif";
	    return undef;
	}
	$ctrlif = <CIF>;
	close(CIF);
	chomp $ctrlif;
    }
    return $ctrlif;
}

#
# Create a map from Emulab lan name to local interface for all experimental
# lans that this node is a member of.
#
sub get_ifmap(;$) {
    my ($fullinfo,) = @_;
    $fullinfo ||= 0;

    # Have we generated the interface mappings already?
    if (scalar(keys %ifmap)) {
	return %ifmap;
    }

    my @ifconfigs = ();
    if (getifconfig(\@ifconfigs) != 0) {
	warn "Could not fetch Emulab interfaces configuration!";
	return undef;
    }

    foreach my $ifconfig (@ifconfigs) {
	my $ip  = $ifconfig->{IPADDR};
	my $mac = $ifconfig->{MAC};
	my $lan = $ifconfig->{LAN};

	next unless $mac && $lan;

	print "Debug: checking interface: $mac/$ip/$lan\n"
	    if $debug;

	my $iface = `$FINDIF -m $mac`;
	chomp $iface;
	if ($? != 0 || !$iface) {
	    warn "Emulab's findif tool failed for ip address: $ip\n";
	    next;
	}
	$ifconfig->{IFACE} = $iface;

	if ($fullinfo) {
	    $ifmap{$lan} = $ifconfig;
	}
	else {
	    $ifmap{$lan} = $iface;
	}
    }

    return %ifmap;
}

#
# Fetch the set of Emulab users and conjure a screen RC file for root with
# appropriate ACLs.
#
sub mk_screenrc() {
    my $firstuser = "";
    my %PWDDB = ();
    
    if (-e $SCREENRC_FILE) {
	if (!unlink $SCREENRC_FILE) {
	    warn "Could not unlink GNU SCREEN RC file: $!\n";
	    return 0;
	}
    }

    open(SCREENRC, ">$SCREENRC_FILE") or 
	die "Could not open $SCREENRC_FILE: $!";

    print SCREENRC "multiuser on\n";

    # Open up the Emulab user database hash and read out all the users,
    # adding them to root's SCREEN RC file (ACLs).
    dbmopen(%PWDDB, $PASSDB, 0) or
	die("Cannot open $PASSDB: $!");
    while (my ($user, undef) = each %PWDDB) {
	print "Debug: Adding user $user to screen RC ACLs.\n"
	    if $debug;
	# The first user will get an explicit permissions entry.
	if (!$firstuser) {
	    $firstuser = $user;
	    print SCREENRC "aclchg $user +rwx '#detach,#copy,#stuff,#meta'\n";
	}
	# Subsequent users will simply inherit the permission of the
	# initial user.
	else {
	    print SCREENRC "aclgrp $user $firstuser\n";
	}
    }

    dbmclose(%PWDDB);
    close SCREENRC;
    return 1;
}

#
# Update variables in the OpenEPC build configuration file
#
sub update_openepc_vars($$) {
    my ($var, $val) = @_;

    # Create empty config file if it doesn't exist.
    if (!-e $OPENEPC_VARFILE) {
	if (system("touch $OPENEPC_VARFILE") != 0) {
	    warn "Could not create $OPENEPC_VARFILE";
	    return 0;
	}
    }

    # Process the config file, looking for the variable to update.
    if (!-r $OPENEPC_VARFILE or !open(OVARS, "< $OPENEPC_VARFILE")) {
	warn "Could not open $OPENEPC_VARFILE for input."; 
	return 0;
    }
    if (!open(NVARS, "> ${OPENEPC_VARFILE}.new")) {
	warn "Can't open ${OPENEPC_VARFILE}.new for writing!";
	close(OVARS);
	return 0;
    }
    my $replaced = 0;
    while (my $ln = <OVARS>) {
	chomp $ln;
	if ($ln =~ /(.*)="(.*)"/) {
	    my ($cvar, $cval) = ($1, $2);
	    if ($cvar eq $var) {
		$ln = "${var}=\"${val}\"";
		$replaced = 1;
	    }
	}
	print NVARS "$ln\n"; 
    }
    close(OVARS);
    if (!$replaced) {
	print NVARS "\n${var}=\"${val}\"\n";
    }
    close(NVARS);

    # Move modified config file into place.
    if (!rename("${OPENEPC_VARFILE}.new", $OPENEPC_VARFILE)) {
	warn "Could not move updated configuration file into place!";
	return 0;
    }

    return 1;
}

#
# Load the GENI manifest
#
sub load_manifest() {
    my $dom;

    # Get boss server's hostname
    my $bossname = tmccbossname();

    my $manistr = `$GENIGET -s $bossname manifest`;
    chomp $manistr;
    if ($? || !$manistr) {
	warn "Error fetching experiment manifest, or no manifest available!\n";
	return undef;
    }

    eval { $dom = XML::LibXML->load_xml(string => $manistr,
					load_ext_dtd => 0, 
					no_network => 1,
					line_numbers => 1) };

    if ($@) {
	warn "Invalid XML in manifest: $@\n";
	return undef;
    }

    return $dom;
}

#
# Extract geni-lib script parameters out of manifest.
#
sub get_geni_parameters() {
    my %rparams = ();

    # Grab the GENI manifest.
    my $mani = load_manifest();
    return undef
	if (!$mani);

    # Grab the manifest's root element.
    my $root = $mani->documentElement();

    # Ugh... XML namespaces!
    my $xc = XML::LibXML::XPathContext->new( $root );
    $xc->registerNs("pns", $GENI_PARAM_NS);
    my @gparams = $xc->findnodes("//pns:data_item");

    # Iterate and pull out parameters.
    foreach my $param (@gparams) {
	my $pname = $param->getAttribute("name");
	my $pval = $param->textContent();
	if ($pname && $pval) {
	    $pname =~ /${GENI_PARAM_PREFIX}\.(\S+)/;
	    $rparams{$1} = $pval;
	}
    }

    return %rparams;
}

#
# Populate OpenEPC subscribers based on UEs in experiment.
#
sub populate_hss_subscribers() {
    return 1
	if (-e $POPULATE_HSS_FFILE);

    # Grab the GENI manifest.
    my $mani = load_manifest();
    return 0
	if (!$mani);

    # Grab the manifest's root element.
    my $root = $mani->documentElement();

    # Process any real UE entries.
    my @nodes = $root->getChildrenByLocalName("node");
    foreach my $node (@nodes) {
	my $vname = $node->getAttribute("client_id");
	my $imsi  = $node->getAttribute("sim_imsi");
	my $sqn   = $node->getAttribute("sim_sequence_number");
	next unless ($vname && $imsi && $sqn);

	print "Provisioning UE '$vname': $imsi, $sqn\n";

	if (defined($imsi) && defined($sqn)) {
	    if (length($imsi) != $IMSI_LENGTH || $imsi !~ /^\d+(\d{4})$/) {
		warn "SIM IMSI attribute for $vname does not look like an IMSI: $imsi\n";
	        next;
	    }
	    my $imsi_id = $1;
	    my $msisdn = "${MSISDN_PREFIX}$imsi_id";

	    if ($sqn !~ /^\d+$/ || length($sqn) > $SIM_SQN_LENGTH) {
		warn "SIM Sequence number for $vname does not look right: $sqn\n";
		next;
	    }
	    $sqn = sprintf("%0${SIM_SQN_LENGTH}s", $sqn);

	    if (system("$ADD_SUBSCRIBER $vname $imsi $sqn $msisdn $imsi_id") != 0) {
		warn "Failed to add subscriber $vname to HSS!\n";
		return 0;
	    }
	}
    }

    # Add entries for simulated UEs.
    my %params = get_geni_parameters();
    if (exists($params{'NUMCLI'}) && int($params{'NUMCLI'}) > 0) {
	my $numcli = $params{'NUMCLI'};
	for (my $i = 1; $i <= $numcli; $i++) {
	    my $imsi_id = $SIM_IMSI_SUFFIX_BASE + $i;
	    my $imsi = sprintf("${PNET_PLMNID}%0${IMSI_SUFFIX_LEN}d",
			       $imsi_id);
	    my $sqn = sprintf("%0${SIM_SQN_LENGTH}d", $SIM_INIT_SQN);
	    my $msisdn = "${MSISDN_PREFIX}$imsi_id";
	    my $vname = "client$i";

	    print "Provisioning simulated UE '$vname': $imsi, $sqn\n";

	    if (system("$ADD_SUBSCRIBER -s $vname $imsi $sqn $msisdn $imsi_id") != 0) {
		warn "Failed to add simulated subscriber $vname to HSS!\n";
		return 0;
	    }
	}
    }

    system("$TOUCH $POPULATE_HSS_FFILE");

    return 1;
}

# Perl goo
1;
