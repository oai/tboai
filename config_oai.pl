#!/usr/bin/perl -w

#
# This script configures OAI for operation in the POWDER / PhantomNet testbeds.
# It is meant to be run on OAI eNB and EPC hosts (with the appropriate ROLE
# argument passed in).
#

use strict;
use English;
use Getopt::Std;
use FindBin;
use Socket;

# Enable file output autoflush
$| = 1;

# PhantomNet and OAI libraries
use lib "$FindBin::Bin/../lib";
use oaipaths;
use epclib;
use oailib;

# Function prototypes
sub gatherInfo();
sub lookupIP(@);
sub initOAIDB();
sub provisionUEs();
sub blockCnetVMARP();
sub setHostName();
sub replaceMacro($$);
sub configureOAI();
sub startOAIService($);
sub runOAI();

# Global vars to store config info used throughout the script.
my $ctrlif;
my %ifaces = ();
my $hname;
my $realm;
my $enb_id;
my %interfaces = ();
my %ipaddrs = ();
my %ipmasks = ();
my %fqdns = ();

# Constants
mt $FFDIR = "/var/tmp";
my $OAI_ETCDIR = "$FindBin::Bin/../etc";
my $OAI_INIT_SQL = "$OAI_ETCDIR/oai_init_db.sql";
my $OAI_CONFIG_FF = "$FFDIR/OAI_CONFIG_DONE";
my $OAI_DB_INIT_FF = "$FFDIR/OAI_DB_INIT_DONE";
my $OAI_PROVISION_FF = "$FFDIR/OAI_UE_PROVISIONING_DONE";
my $EMULAB_VMRANGE = "172.16.0.0/12";

my $TOUCH = "/usr/bin/touch";
my $CAT = "/bin/cat";
my $HOSTNAME = "/bin/hostname";
my $ARPTABLES = "/sbin/arptables";

my %OAI_ROLES = ( 
    # Combined EPC role on one node (HSS, MME, SPGW)
    'EPC' => 1,
    # SDR eNodeB role
    'ENB' => 1,
    # Emulated eNodeB + UE
    'SIM_ENB' => 1,
);

my %OAI_CONFFILES = (
    'EPC' => ['hss.conf', 'mme.conf', 'spgw.conf', 'hss_fd.conf', 'mme_fd.conf'],
    'ENB' => ['enb.conf'],
    'SIM_ENB' => ['enb.conf'],
);

my @REPLACE_PATS = (
    { PATTERN => qr/(%(\w+?)_IPMASK%)/, FUNC => \&replaceMacro, MAP => \%ipmasks },
    { PATTERN => qr/(%(\w+?)_IP%)/, FUNC => \&replaceMacro, MAP => \%ipaddrs },
    { PATTERN => qr/(%(\w+?)_INTF%)/, FUNC => \&replaceMacro, MAP => \%interfaces },
    { PATTERN => qr/(%(\w+?)_FQDN%)/, FUNC => \&replaceMacro, MAP => \%fqdns },
    { PATTERN => qr/(%REALM%)/, FUNC => \&replaceMacro, REPLSTR => \$realm },
    { PATTERN => qr/(%HSS_OPKEY%)/, FUNC => \&replaceMacro, REPLSTR => \$PNET_OPKEY },
    { PATTERN => qr/(%HOSTNAME%)/, FUNC => \&replaceMacro, REPLSTR => \$hname },
    { PATTERN => qr/(%MCC%)/, FUNC => \&replaceMacro, REPLSTR => \$PNET_MCC },
    { PATTERN => qr/(%MNC%)/, FUNC => \&replaceMacro, REPLSTR => \$PNET_MNC },
    { PATTERN => qr/(%TAC%)/, FUNC => \&replaceMacro, REPLSTR => \$DEF_TAC },
    { PATTERN => qr/(%ENB_ID%)/, FUNC => \&replaceMacro, REPLSTR => \$enb_id },
);

my %ROLEDEFS = (
    'EPC' => {
	'S11_MME_INTF' => 'lo',
	'S11_MME_IP' => '127.0.11.1',
	'S11_MME_IPMASK' => '127.0.11.1/8',
	'S11_SGW_INTF' => 'lo',
	'S11_SGW_IP' => '127.0.11.2',
	'S11_SGW_IPMASK' => '127.0.11.2/8',
	'S6A_HSS_INTF' => 'lo',
	'S6A_HSS_IP' => '127.0.0.1',
	'S6A_HSS_IPMASK' => '127.0.0.1/8',
    },

    'ENB' => {},
    'SIM_ENB' => {},
);

my @MME_HOSTLIST = ('mme-s1-lan', 'epc-s1-lan');

#
# Display help and exit.
#
sub help() {
    print "Usage: config_oai [-n] [-d] -r <role>\n";
    print "  -r : OAI role.  One of: ". join(", ", keys %OAI_ROLES) ."\n";
    print "  -d : Enable debugging messages.\n";

    exit 1;
}

#
# Enforce running script as root.
#
($UID == 0)
    or die "You must run this script as root (e.g., via sudo)!\n";

#
# Parse command line arguments.
#
my %opts = ();
if (!getopts("dr:",\%opts)) {
    help();
}

my $role        = $opts{'r'}
    or do { warn "Must specify OAI role!\n"; help() };
my $debug       = $opts{'d'} ? 1 : 0;

if (!exists($OAI_ROLES{$role})) {
    warn "Invalid role specified: $role\n";
    exit 1;
}

#
# Top-level logic flow.
#
gatherInfo();
configureOAI();
TOPSW: for ($role) {
    /^EPC$/ && do {
	initOAIDB();
	provisionUEs();
	blockCnetVMARP();
	last TOPSW;
    };

    /^ENB$/ && do {
	# Nothing to do here.
	last TOPSW;
    };

    /^SIM_ENB$/ && do {
	# Nothing to do here.
	last TOPSW;
    };


    # Default
    die "Unknown role: $role\n";
}
runOAI();

#
# Helper to lookup a list of hosts, searching for an IP.
#
sub lookupIP(@) {
    my (@hlist) = @_;
    my $ipaddr = undef;

    foreach my $name (@hlist) {
	my $res = gethostbyname($name);
	if ($res) {
	    $ipaddr = inet_ntoa($res);
	    last;
	}
    }

    return $ipaddr;
}

#
# Gather information about the experiment/environment.  Leave the results
# in global variables.
#
sub gatherInfo() {
    $hname = setHostName();
    $ctrlif = get_emulab_controlif();
    %ifaces = get_ifmap(1);
    $realm = $PNET_REALM; # XXX: hardcoding is not awesome.

    SW1: for ($role) {
	# Combined core node (HSS, MME, and SPGW).
	/^EPC$/ && do {
	    $interfaces{'SGI_PGW'} = $ctrlif;
	    $interfaces{'S1_MME'} = $ifaces{'s1-lan'}->{'IFACE'};
	    $ipaddrs{'S1_MME'} = $ifaces{'s1-lan'}->{'IPADDR'};
	    $ipmasks{'S1_MME'} = $ipaddrs{S1_MME} . '/24'; # XXX: hardcoding.
	    $interfaces{'S1U_SGW'} = $ifaces{'s1-lan'}->{'IFACE'};
	    $ipaddrs{'S1U_SGW'} = $ifaces{'s1-lan'}->{'IPADDR'};
	    $ipmasks{'S1U_SGW'} = $ipaddrs{S1U_SGW} . '/24'; # XXX: hardcoding.
	    $interfaces{'S11_MME'} = $ROLEDEFS{'EPC'}->{'S11_MME_INTF'};
	    $ipaddrs{'S11_MME'} = $ROLEDEFS{'EPC'}->{'S11_MME_IP'};
	    $ipmasks{'S11_MME'} = $ROLEDEFS{'EPC'}->{'S11_MME_IPMASK'};
	    $interfaces{'S11_SGW'} = $ROLEDEFS{'EPC'}->{'S11_SGW_INTF'};
	    $ipaddrs{'S11_SGW'} = $ROLEDEFS{'EPC'}->{'S11_SGW_IP'};
	    $ipmasks{'S11_SGW'} = $ROLEDEFS{'EPC'}->{'S11_SGW_IPMASK'};
	    $interfaces{'S6A_HSS'} = $ROLEDEFS{'EPC'}->{'S6A_HSS_INTF'};
	    $ipaddrs{'S6A_HSS'} = $ROLEDEFS{'EPC'}->{'S6A_HSS_IP'};
	    $ipmasks{'S6A_HSS'} = $ROLEDEFS{'EPC'}->{'S6A_HSS_IPMASK'};
	    $fqdns{'MME'} = "$hname.$realm";
	    $fqdns{'HSS'} = "hss.$realm";
	    last SW1;
	};

	# SDR eNodeB node.
	/^ENB$/ && do {
	    $interfaces{'S1_ENB'} = $ifaces{'s1-lan'}->{'IFACE'};
	    $ipaddrs{'S1_ENB'} = $ifaces{'s1-lan'}->{'IPADDR'};
	    $ipmasks{'S1_ENB'} = $ipaddrs{S1_ENB} . '/24'; # XXX: hardcoding.
 	    $interfaces{'S1U_ENB'} = $ifaces{'s1-lan'}->{'IFACE'};
	    $ipaddrs{'S1U_ENB'} = $ifaces{'s1-lan'}->{'IPADDR'};
	    $ipmasks{'S1U_ENB'} = $ipaddrs{S1_ENB} . '/24'; # XXX: hardcoding.
	    $ipaddrs{'S1_MME'} = lookupIP(@MME_HOSTLIST);
	    $hname =~ /(\d+)$/;
	    $enb_id = "0x" . $1;
	    last SW1;
	};

	# EMULATED (OAISIM) eNodeB node, exactly the same configuration as SDR eNodeB.
	/^SIM_ENB$/ && do {
	    $interfaces{'S1_ENB'} = $ifaces{'s1-lan'}->{'IFACE'};
	    $ipaddrs{'S1_ENB'} = $ifaces{'s1-lan'}->{'IPADDR'};
	    $ipmasks{'S1_ENB'} = $ipaddrs{S1_ENB} . '/24'; # XXX: hardcoding.
 	    $interfaces{'S1U_ENB'} = $ifaces{'s1-lan'}->{'IFACE'};
	    $ipaddrs{'S1U_ENB'} = $ifaces{'s1-lan'}->{'IPADDR'};
	    $ipmasks{'S1U_ENB'} = $ipaddrs{S1_ENB} . '/24'; # XXX: hardcoding.
	    $ipaddrs{'S1_MME'} = lookupIP(@MME_HOSTLIST);
	    $hname =~ /(\d+)$/;
	    $enb_id = "0x00e";  #XXX: hardcoding.
	    last SW1;
	};

	# Default
	die "Unknown role: $role\n";
    }
}

#
# Stop control network ARP requests for the VM control network from
# getting through to OAI and/or the GTP tunnel it sets up.  Otherwise
# it may end up stealing the VM control network IP.
#
sub blockCnetVMARP() {
    # system("$ARPTABLES -F") == 0
    #	 or die "Could not flush arptables!\n";
    system("$ARPTABLES -I INPUT -i $ctrlif -d $EMULAB_VMRANGE -j DROP") == 0
	or die "Failed to install VM control network ARP block rule!\n";
}

#
# Set the hostname to non-cannonical form of testbed nickname. We
# need to do this for FreeRadius.
#
sub setHostName() {
    my $nick = `$CAT $BOOTDIR/nickname`;
    chomp $nick;
    my ($sname,) = split(/\./, $nick);
    system("$HOSTNAME $sname") == 0
	or die "Could not set hostname to: $sname\n";
    return $sname;
}

#
# Generic macro replacement helper function.
#
sub replaceMacro($$) {
    my ($cline, $pat) = @_;
    $cline =~ $pat->{PATTERN};
    my $macro = $1;
    my $epc_func = $2;
    my $repl = "";
    if (exists($pat->{MAP})) {
	$repl = $pat->{MAP}->{$epc_func};
    }
    elsif (exists($pat->{REPLSTR})) {
	$repl = ${$pat->{REPLSTR}};
    }
    $cline =~ s/$macro/$repl/g;

    return $cline;
}

#
# Do all of the things for OAI
#
sub configureOAI() {
    # Already done?
    return if (-e $OAI_CONFIG_FF);

    # Macro-replace and install OAI config files.
    foreach my $cfile (@{$OAI_CONFFILES{$role}}) {
	my $destdir = $OAI_SYSETC;
	if ($cfile =~ /_fd\.conf$/) {
	    $destdir .= "/freeDiameter";
	}
	open(SFILE, "<$OAI_ETCDIR/$cfile")
	    or die "Could not open source OAI config file: $OAI_ETCDIR/$cfile\n";
	open(TFILE, ">$destdir/$cfile")
	    or die "Could not open target OAI config file: $OAI_SYSETC/$cfile\n";
	while (my $cline = <SFILE>) {
	    chomp $cline;
	    foreach my $pat (@REPLACE_PATS) {
		if ($cline =~ $pat->{PATTERN}) {
		    $cline = $pat->{FUNC}($cline, $pat);
		}
	    }
	    if (defined($cline)) {
		print TFILE $cline . "\n";
	    }
	}
	close(SFILE);
	close(TFILE);
	print "Updated/installed OAI config file: $cfile\n";
    }

    # Role-specific tasks.
    ROLESW: for ($role) {
	/^EPC$/ && do {
	    # Generate freeDiameter certificates.
	    system("$OAI_EPCDIR/SCRIPTS/check_mme_s6a_certificate $OAI_SYSETC/freeDiameter $fqdns{MME} > $OAI_LOGDIR/check_mme_cert.log 2>&1") == 0
		or die "Failed to generate freeDiameter certs for MME!\n";
	    system("$OAI_EPCDIR/SCRIPTS/check_hss_s6a_certificate $OAI_SYSETC/freeDiameter $fqdns{HSS} > $OAI_LOGDIR/check_hss_cert.log 2>&1") == 0
		or die "Failed to generate freeDiameter certs for HSS!\n";
	    last ROLESW;
	};
	
	/^ENB$/ && do {
	    # Nothing else to do presently!
	    last ROLESW;
	};
	/^SIM_ENB$/ && do {
	    # Nothing else to do presently!
	    last ROLESW;
	};

    }
    
    # Mark that we're done.
    system("$TOUCH $OAI_CONFIG_FF") == 0 or
	die "Could not create OAI configuration flag file!";
}

#
# Initialize OAI database
#
sub initOAIDB() {
    # Already done?
    return if (-e $OAI_DB_INIT_FF);
    
    DBImport($OAI_DB_USER, $OAI_DB_PASS, $OAI_DB_NAME, $OAI_INIT_SQL);

    # Mark one-time action as complete.
    system("$TOUCH $OAI_DB_INIT_FF") == 0 or
	die "Could not touch DB init flag file!";
    
    print "OAI DB initialized.\n";
    return;
}

#
# Put the UEs allocated to this experiment into the OAI database.
#
sub provisionUEs() {

    # Already done?
    return if (-e $OAI_PROVISION_FF);
    
    # Connect to the OAI DB.
    my $dbh = connectDB($OAI_DB_USER, $OAI_DB_PASS, $OAI_DB_NAME)
	if !$dbh;
    
    # Add top-level MME record.
    $mme_id = get_max_id("mmeidentity", "idmmeidentity") + 1;
    $dbh->do("INSERT INTO mmeidentity VALUES ($mme_id, '$fqdns{MME}', '$realm', 0)");

    # Add top-level PGW record.
    $pgw_id = get_max_id("pgw") + 1;
    my $ipv6 = "0:0:0:0:0:0:0:$pgw_id"; # XXX: completely bogus.
    $dbh->do("INSERT INTO pgw VALUES ($pgw_id, '$ipaddrs{S1U_SGW}', '$ipv6')");

    # Add real UE devices.
    provisionRealUEs($fqdns{'MME'}, $ipaddrs{'S1U_SGW'}, $realm);

    # Add simulated UEs.
    provisionSimUEs($fqdns{'MME'}, $ipaddrs{'S1U_SGW'}, $realm);

    # Mark provisioning as done.
    system("$TOUCH $OAI_PROVISION_FF") == 0 or
	die "Could not create OAI provisioning flag file!";
}

#
# Start up an individual OAI service.
#
sub startOAIService($) {
    my ($svcname) = @_;

    print "Starting OAI service: $svcname\n";

    system("$OAI_BINDIR/${svcname}.start.sh") == 0
	or warn "Could not start service: $svcname\n";

    return $?;
}

#
# Start up OAI components for our role.
#
sub runOAI() {
    ROLESW: for ($role) {
	/^EPC$/ && do {
	    startOAIService("hss");
	    startOAIService("mme");
	    startOAIService("spgw");
	    last ROLESW;
	};

	/^ENB$/ && do {
	    startOAIService("enb");
	    last ROLESW;
	};

	/^SIM_ENB$/ && do {
	    startOAIService("sim_enb");
	    last ROLESW;
	};

	# Default
	die "Unknown role: $role\n";
    }

    print "OAI services started.\n";
}
