#!/usr/bin/perl -w

#
# This script adds all UEs found in the experiment manifest to the OAI DB.
#

use strict;
use English;
use Getopt::Std;
use FindBin;

# Enable file output autoflush
$| = 1;

# Import OAI library
use lib "$FindBin::Bin/lib";
use oailib;

sub help() {
    print "Usage: $0 [-d] -m <MME hostname> -r <FreeRADIUS Realm> -p <SPGW IP address>\n";
    exit 1;
}

#
# Parse command line arguments.
#
my %opts = ();
if (!getopts("dm:r:p:",\%opts)) {
    help();
}

my $debug     = $opts{'d'} ? 1 : 0;
my $mmename   = $opts{'m'}
    or do { warn "Must specify the MME's hostname!\n"; help() };
my $realm     = $opts{'r'}
    or do { warn "Must specify the FreeRADIUS realm!\n"; help() };
my $pgwip     = $opts{'p'}
    or do { warn "Must specify the SPGW's IP address!\n"; help() };

my $mmefqdn = "$mmename.$realm";

provisionRealUEs($mmefqdn, $pgwip, $realm);
