#!/bin/bash

OAIRANDIR="/opt/oai/openairinterface5g"
ENBEXE="lte-softmodem.Rel10"
ENBEXEPATH="$OAIRANDIR/targets/bin/$ENBEXE"
ENBCONFPATH="/usr/local/etc/oai/enb.conf"

cd /var/tmp

# Kill off running function.
killall -q $ENBEXE
sleep 1

# Startup function.
screen -S enb -d -m -h 10000 /bin/bash -c "$ENBEXEPATH -O $ENBCONFPATH"

# Do some cleanup.
screen -wipe >/dev/null 2>&1

exit 0
